### Get oriented: ###
* You can get oriented with the code by running scripts from the demos directory:
	- `demos/demo_build_PRISM.sh` (Takes a little while)
	- `demos/demo_build_MGIPhenotype.sh` (Takes even longer)
	- `demos/demo_run.sh`

### To Do List: ###

#### Speed ####
A single data set run takes longer then I would like O(1 min). There are two main bottlenecks.

* **Data import**:
  The loading of the precomputed sparse matricies from the npz files takes a while.
	- This could be improved by looking into more efficient uncompressed storage techniques.
	- This could also be fixed by running as a server and keeping data in ram.

* **Hypergeometric SF**:
  The scipy implementation of this has issues. It is slow (about 100 times slower than R supposedly https://github.com/scipy/scipy/issues/4982) and also suffers from underflow.
  As this is a major bottleneck (30s or so) might be nice to look into better implementations.

#### run.py ####

* p value:
	- We would like to give a reasonable p-value for our final list of enriched TFs. This needs to be done.
	  I have some ideas.

* Underflow:
  gen_terms can produce term list in a variety of ways. My current preference is
	the bin(omial) term generation method. I have to use a high precision library
	to catch the binomial underflow in the log survival function. It would be nice
	if this could be dealt with in a speedier / cleaner way.

* Features:
	- Hyper p-value
		- This is quite slow (~30s compared to .1 for binomial p value). Is there a way to speed this up? It, and loading data
		  are the main speed bottlenecks. Runtime is about 1 min total. It would be nice to bring this down.
		- I would like to compute this 2 ways and see if it matters.
			- Treat null (ie N and K) as coming from all TFs. This is done.
			- Remove TF of interest when computing null (this is what we did previously)
			   TO DO: only the first is implemented correctly now. For the second we need to filter out terms
			   where it is a single TF which contributes. (Note these cases occur, which is a bit funny right that means
			   one TF is pulling a lot of weight, perhaps worth exploring)
	- Other features
	  We currently use binomial p-value and before jump features in our filter.
	  It may be easier (better?) to use the hyper within all tfbs feature described bellow.

#### Binary ####

I have begun writing a bin/whichTF.py script.

* **Distribution**:
  I would like this to eventually be converted to a bin/whichTF executable. Ideally there would be two ways to get this:
	- **Clone source tree**:
	  This would give the code and the binary. Perhaps http://python-packaging.readthedocs.io/en/latest/command-line-scripts.html
	  is a good guide to best practices for this approach.

	- **Forzen binary**:
	  Simple binary for users who don't want to code and just want to run the program.

* **Default mode** (run many times):
	- whichTF [options] data_set assembly (Default run, generates list of TFs with score, p-value, corrected p-value)
		- This is built and has been tested.
		- Current default term method is GREATER. I would prefer switching this to bin(omial).

* **buildTFBS** (run once for each tfbs set):
	- whichTF [options] --buildTFBS assembly tfbs_name tfbs_dir
	  This builds tfbs files from a directory containing all sites (ie the PRISM directory).
	  	- This is built and has been tested.
* **buildOnt** (run once for each ontology *have to run again for new tfbs set*):
	- whichTF --buildOnt ontoToGene.canon assembly
	  Build data associated with assembly and ontology
	  	- This is built and has been tested.

#### Web Interface ####

We also need to build a web interface. I imagine this would have a simple data submission and assembly dropdown.
All other settings could be tweaked, but would be hidden (ie need to click dropdown arrow or something).
Otherwise they are set to their default values.

#### GitHub Page ####
Would be great to push this to GitHub and have a GitHub pages page for it and results in paper.

### Conceptual points (perhaps for v2): ###
#### Features ####
* Hyper between terms:
	- This treats all tfbs tracks that overlap the query as a null. It then
	computes the enrichment for each term and TF. NOTE: It does not use all tfbs
	as null, ie it is only for ranking within query.
	- This can be computed either by holding out the given TF when computing the
	null, or leaving it in.
* Binomial for each term
	- This uses a uniform prob on the genome, and computes:
	p = len(reg_dom(term)) / len(genome). It can be used to compare between terms.

These are the two main statistical features used in our prototype. The former is
dependent on the query, so it is not good at say ranking terms within the query,
as the null is wrong for that. The second can in principle be used for this, but
it does not take advantage of the fact that we are picking a subset of some given
tfbs set. One additional feature to potentially play with.

* Hyper within all tfbs:
	- This seems natural as:
		- It takes advantage of the true tfbs background.
		- It takes advantage of larger reg domains hitting more sites.
	- One challenge is that as currently implemented, the PRISM sites are highly
	redundant, and overlapping. Doing this properly would, at the very least, require
	a merge. In the future, it might be nice to develop a canonical non-overlapping
	set of tfbs sites.
