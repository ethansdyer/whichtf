import os, sys
from urllib.parse import unquote_plus


from flask import Flask
from werkzeug.utils import secure_filename

import sample_external_prog as prog

from whichtf.run import run_main


app = Flask(__name__)


def set_settings():
    settings = dict([])

    settings['GREATER']='GREATER'
    settings['_whichTFRoot']='/cluster/u/ytanigaw/repos/whichtf'
    settings['_tmpRoot'] = '/cluster/u/ytanigaw/tmp/WhichTF-flask'
    settings['assembly']='hg19'
    settings['bedtools']='bedtools'
    settings['before_bin_jump']=True
    settings['before_hyp_jump']=True
    settings['bin_jump_thresh']=0
    settings['data']='data'
    settings['equal_first_n']=1
    settings['first_n_strict']=False
    settings['getRegDoms']='getRegDoms.py'
    settings['hyp_jump_thresh']=0
    settings['inFile']='data/demo/ENCFF719GOE.bed'
    settings['leaveTrace']=False
    settings['listAssemblies']=False
    settings['max_rank']=1000
    settings['max_tf']=10
    settings['min_score']=0
    settings['myMax']=500
    settings['myMin']=2
    settings['myOnt']='MGIPhenotype'
    settings['outFile']='data/demo/ENCFF719GOE.whichtf.tsv'
    settings['overlapSelect']='overlapSelect'
    settings['settings']='.WTF_settings'
    settings['termMethod']='GREATER'
    settings['termNum']=100
    settings['tfbs']='PRISM'
    settings['verbose']=True

    return settings


@app.route('/')
def hello_world():
    return 'Hello, World!'

@app.route('/run_stable/<path:quotedAbsInputFile>')
def run_stable(quotedAbsInputFile):
    inputFile = os.sep + unquote_plus(quotedAbsInputFile)
    outFile = prog.run(inputFile)
    return outFile

@app.route('/run/<path:quotedAbsInputFile>')
def run(quotedAbsInputFile):

    settings            = set_settings()
    settings['inFile']  = os.sep + unquote_plus(quotedAbsInputFile)
    settings['outFile']  = os.path.join('/cluster/u/ytanigaw/tmp/WhichTF-flask', secure_filename(settings['inFile']))
    run_main(settings)

    return settings['outFile']
