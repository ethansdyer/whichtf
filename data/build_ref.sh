#!/bin/bash
set -beEuo pipefail

SRCNAME=$(readlink -f $0)
SRCDIR=$(dirname ${SRCNAME})
PROGNAME=$(basename $SRCNAME)
VERSION="0.0.1"
NUM_POS_ARGS="2"

############################################################
# functions
############################################################

show_default_helper () {
    cat ${SRCNAME} | grep -n Default | tail -n+3 | awk -v FS=':' '{print $1}' | tr "\n" "\t"
}

show_default () {
    cat ${SRCNAME} \
        | tail -n+$(show_default_helper | awk -v FS='\t' '{print $1+1}') \
        | head  -n$(show_default_helper | awk -v FS='\t' '{print $2-$1-1}')
}

usage () {
cat <<- EOF
	$PROGNAME (version $VERSION)
	This script calls WhichTF buildTFBS and buildOnt and prepare the reference dataset for a given assembly

	Usage: $PROGNAME [options] assembly
	  assembly           the assembly

	Options:
	  --ontology   (-o)  the ontology name
	  --assembly_v       the assembly version
	  --ontology_v       the ontology version
	  --prism_d          the location of PRISM tracks

	Default configurations:
EOF
    show_default | awk -v spacer="  " '{print spacer $0}'
}

############################################################
# tmp dir
############################################################
tmp_dir="$(mktemp -d tmp-$(basename $0)-$(date +%Y%m%d-%H%M%S)-XXXXXXXXXX)"
handler_exit () { rm -rf $tmp_dir ; }
trap handler_exit EXIT

############################################################
# parser start
############################################################
## == Default parameters (start) == ##
ontology=MGIPhenotype
assembly_v=20171029
ontology_v=20171029
getRegDoms=/cluster/bin/getRegDoms.py
prism_d=__AUTO__
## == Default parameters (end) == ##

declare -a params=()
for OPT in "$@" ; do
    case "$OPT" in
        '-h' | '--help' )
            usage >&2 ; exit 0 ;
            ;;
        '-v' | '--version' )
            echo $VERSION ; exit 0 ;
            ;;
        '-a' | '--assembly' )
            assembly=$2 ; shift 2 ;
            ;;
        '-o' | '--ontology' )
            ontology=$2 ; shift 2 ;
            ;;
        '--assembly_v' )
            assembly_v=$2 ; shift 2 ;
            ;;
        '--ontology_v' )
            ontology_v=$2 ; shift 2 ;
            ;;
        '--getRegDoms' )
            getRegDoms=$2 ; shift 2 ;
            ;;
        '--prism_d' )
            prism_d=$2 ; shift 2 ;
            ;;
        '--'|'-' )
            shift 1 ; params+=( "$@" ) ; break
            ;;
        -*)
            echo "$PROGNAME: illegal option -- '$(echo $1 | sed 's/^-*//')'" 1>&2 ; exit 1
            ;;
        *)
            if [[ $# -gt 0 ]] && [[ ! "$1" =~ ^-+ ]]; then
                params+=( "$1" ) ; shift 1
            fi
            ;;
    esac
done

if [ ${#params[@]} -lt ${NUM_POS_ARGS} ]; then
    echo "${PROGNAME}: ${NUM_POS_ARGS} positional arguments are required" >&2
    usage >&2 ; exit 1 ;
fi

assembly="${params[0]}"
############################################################

# configure paths based on the paramters
loci_f=/cluster/data/ontologies/${assembly}/${assembly_v}/${assembly}.loci
chrom_sizes=$(dirname ${loci_f})/chrom.sizes
onto_to_gene=$(dirname ${loci_f})/${ontology}/${ontology_v}/ontoToGene.canon

if [ ${prism_d} == "__AUTO__" ] ; then
    prism_d=/cluster/u/edyer/prj/tfFlagger/data/PRISM/${assembly}/gene/top5k
fi

WhichTF buildTFBS ${assembly} PRISM ${prism_d}

cp ${chrom_sizes} ${SRCDIR}/${assembly}/

WhichTF buildOnt --getRegDoms ${getRegDoms} ${assembly} ${loci_f} ${ontology} ${onto_to_gene}
