import os
import sys
import subprocess
import glob
import gzip
import csv
import numpy as np
from scipy import sparse
from logging import getLogger
import pickle

np.seterr(all='warn')
np.seterr(under='warn')

logger = getLogger("build.py")

def opener(path):
    """
    Open a .gz or regular file respectively.

    Inputs:
        path - path to file.
    Outputs:
        f - file handle.
    """
    if path.split('.')[-1] == 'gz':
        f = gzip.open(path)
    else:
        f = open(path)

    return f

def build_dir_tree(path):
    """
    Given a path build out the associated dir tree.
    Assumes final dir with no trailing / has no . in name.

    Inputs:
        path - Path to file or directory.

    Outputs:
        Creates folders on disk.
    """

    dir_list = path.split('/')

    if '.' in dir_list[-1]:
        dir_list = dir_list[:-1]
    if dir_list[-1] == '':
        dir_list = dir_list[:-1]
    if dir_list[0] == '':
        dir_list = dir_list[1:]
        dir_list[0] = '/' + dir_list[0]

    logger.debug('Building ' + '/'.join(dir_list))

    tree = ''
    for folder in dir_list:
        tree = tree + folder + '/'
        if not os.path.exists(tree):
            logger.debug('Making ' + tree)
            os.makedirs(tree)
        else:
            logger.debug(tree + ' exists. Skipping.')

def process_tfbs(in_path, out_path, tf_name=None, force=False):
    """
    Take either a single bed file, or a directory, and write a file with
    all binding sites, and tf_name appended.

    Inputs:
        in_path -  Either a tfbs bed file or directory containing such files.
        out_path - Path to file containing labeled tfbs.
        tf_name - Use a custom name for tf. Default is taken from name.bed(.gz).
        force - Overwrite out file?

    Outputs:
        Writes a file containing labeled tfbs to disk.
    """
    def process_tfbs_single(in_file, out_path, tf_name = None):
        if(tf_name is None):
            tf_name = os.path.basename(in_file).split('.')[0]
        with opener(in_file) as tfbs_in, open(out_path, 'a') as tfbs_out:
            for line in tfbs_in:
                tfbs_out.write('{}\t{}\n'.format(line.decode().rstrip(), tf_name))

    # check overwrite
    if os.path.exists(out_path):
        if(force == False):
            logger.warning('{} already exists. Please remove or set force=True.'.format(out_path))
        else:
            os.remove(out_path)

    if not os.path.isdir(in_path):
        # if the input is a single file
        process_tfbs_single(in_path, out_path, tf_name)
    else:
        # if the input is a directory,
        # iterate over contents of directory.
        files = glob.glob(os.path.join(in_path, '*.bed*'))
        for bed_file in files:
            process_tfbs_single(bed_file, out_path)

        logger.info('sorting {}'.format(out_path))
        sort = subprocess.call(('sort', '-k1.4,1V', '-k2,2n', '-k3,3n',
                                '-k4,4', out_path, '-o', out_path))


def merge_tfbs(labeled_tfbs, merge_site_file, settings):
    """
    Create a file with the merged sites.

    Inputs:
        labeled_tfbs - Location of labeled tfbs.
        merge_site_file - Location to write merged sites.

    Outputs:
        Writes merged sites to disk.
        sort - exit code of sort.
    """
    # TO DO:
    #   Might want to make it so it doesn't automatically overwrite.

    with open(merge_site_file, 'w') as write_tfbs:
        cut = subprocess.Popen(('cut', '-f1-3', labeled_tfbs),
                               stdout=subprocess.PIPE)
        merge = subprocess.call((settings['bedtools'], 'merge', '-i', 'stdin'),
                                 stdin=cut.stdout, stdout=write_tfbs)
        cut.wait()

        sort = subprocess.call(('sort', '-k1,1', '-k2,2n', '-k3,3n',
                                merge_site_file, '-o', merge_site_file))

    return sort

def unique_tfbs(labeled_tfbs, unique_site_file):
    """
    Create a file with only the unique sites.

    Inputs:
        labeled_tfbs - Location of labeled tfbs.
        unique_site_file - Location to write unique sites.

    Outputs:
        Writes unique sites to disk.
        sort - exit code of sort.
    """
    # TO DO:
    #   Might want to make it so it doesn't automatically overwrite.

    with open(unique_site_file, 'w') as write_tfbs:
        cut = subprocess.Popen(('cut', '-f1-3', labeled_tfbs),
                               stdout=subprocess.PIPE)
        sort = subprocess.call(('sort',
                               '-k1,1',
                               '-k2,2n',
                               '-k3,3n',
                               '-u'), stdin=cut.stdout, stdout=write_tfbs)
        cut.wait()

    return sort

def reg_region_by_term(ont_path, assembly_loci, merge_file, settings,
                       tmp_dir=None):
    """
    Create a tsv of the form region X ontology term

    Inputs:
        ont_path - Path to ontoToGene.canon file of ontology.
        assembly_loci - Assembly .loci file
        merge_file - Output path for merged reg domain X ontology term bed file.
        settings - Settings dictionary.
        tmp_dir - Directory to store temp files.

    Outputs:
        Writes final reg region X ontology term bed file to disk
    """
    if tmp_dir == None:
        rm_folder = True
        tmp_dir = '/'.join(merge_file.split('/')[:-1])+'/tmp'

    if not os.path.exists(tmp_dir):
        os.makedirs(tmp_dir)

    # Sort term to gene file.
    with open(tmp_dir+'/ontoToGene.sorted.canon', 'w') as sorted_ont:
        subprocess.call(('sort', '-k2,2', '-k1,1', ont_path),
                        stdout=sorted_ont)

    # Generate all reg domains for hg19 genes
    subprocess.call((settings['getRegDoms'], settings['assembly'],
                     assembly_loci, tmp_dir+'/reg_dom_tmp.bed'))

    # Cut and sort reg domains.
    with open(tmp_dir+'/reg_dom_tmp.sorted.bed', 'w') as sorted_reg:
        cut = subprocess.Popen(('cut', '-f1-3,5', tmp_dir+'/reg_dom_tmp.bed'),
                               stdout=subprocess.PIPE)
        sort = subprocess.call(('sort', '-k4,4', '-k1,1', '-k2,2n', '-k3,3n'),
                               stdin=cut.stdout, stdout=sorted_reg)
        cut.wait()

    # Create term X reg_dom file (not yet split by tfbs regions, or merged)
    with open(tmp_dir+'/term_by_reg_domains.bed', 'w') as term_by_rd:
        join = subprocess.Popen(('join', '-t', '\t', '-1', '4', '-2', '2',
                                 tmp_dir+'/reg_dom_tmp.sorted.bed',
                                 tmp_dir+'/ontoToGene.sorted.canon'),
                                stdout=subprocess.PIPE)
        cut = subprocess.call(('cut','-f2-5'), stdin=join.stdout,
                              stdout=term_by_rd)

        join.wait()

    # Merge domains for each term
    sort = subprocess.call(('sort', '-u', '-k4,4', '-k1,1', '-k2,2n', '-k3,3n',
                            tmp_dir+'/term_by_reg_domains.bed', '-o',
                            tmp_dir+'/term_by_reg_domains.bed'))

    split_merge(tmp_dir+'/term_by_reg_domains.bed', merge_file, settings,
                tmp_dir=tmp_dir)

    sort = subprocess.call(('sort', '-u', '-k1,1', '-k2,2n', '-k3,3n', '-k4,4',
                            merge_file, '-o', merge_file))

    # Remove temporary files.
    if not settings['leaveTrace']:
        os.remove(tmp_dir+'/ontoToGene.sorted.canon')
        os.remove(tmp_dir+'/reg_dom_tmp.bed')
        os.remove(tmp_dir+'/reg_dom_tmp.sorted.bed')
        os.remove(tmp_dir+'/term_by_reg_domains.bed')
        try:
            os.rmdir(tmp_dir)
        except OSError:
            print('Did not remove %s as it was non-empty.') % tmp_dir

def split_merge(reg_by_term, merged_reg_by_term, settings, tmp_dir=None, verbose = False):
    """
    Takes a bed file with 4th column as id, and performs a merge only over
    regions with the same value for the id.

    Inputs:
        reg_by_term - Input path of labeled reg domains.
        settings - Settings dictionary.
        merged_reg_by_term - Output path for merged file.
        tmp_dir - Temporary folder.
        verbose - Whether or not to output progress.

    Outputs:
        Writes merged file to disk.
    """
    if os.path.exists(merged_reg_by_term):
        os.remove(merged_reg_by_term)

    if tmp_dir == None:
        tmp_dir = '/'.join(reg_by_term.split('/')[:-1])

    if merged_reg_by_term == None:
        merged_reg_by_term = ('.'.join(reg_by_term.split('.')[:-1])
                              + '.merge.bed')

    currentLabel = None

    with open(reg_by_term,'r') as myFile:
        myReader = csv.reader(myFile, delimiter='\t')

        for row in myReader:
            myLabel = row[3]

            if currentLabel == None:
                currentLabel = myLabel
                split_merge_out = tmp_dir + '/' + currentLabel + '.bed'
                split_merge_handle = open(split_merge_out,'w')
                myWriter = csv.writer(split_merge_handle, delimiter='\t')

            if myLabel == currentLabel:
                myWriter.writerow(row)
            else:
                split_merge_handle.close()

                # Merge and append
                with open(merged_reg_by_term, 'a') as merge_file:
                    merge = subprocess.call((settings['bedtools'], 'merge', '-i',
                                             split_merge_out, '-c', '4', '-o',
                                             'distinct'), stdout=merge_file)

                os.remove(split_merge_out)

                currentLabel = myLabel
                split_merge_out = tmp_dir + '/' + currentLabel + '.bed'
                split_merge_handle = open(split_merge_out,'w')
                myWriter = csv.writer(split_merge_handle, delimiter='\t')
                myWriter.writerow(row)

        split_merge_handle.close()

        # Merge and append after eof
        with open(merged_reg_by_term, 'a') as merge_file:
            merge = subprocess.call((settings['bedtools'], 'merge', '-i',
                                     split_merge_out, '-c', '4', '-o',
                                     'distinct'), stdout=merge_file)

        if not settings['leaveTrace']:
            os.remove(split_merge_out)

def reg_dom_size(merged_reg_by_term, rd_sizes, verbose=False):
    """
    Reads in a list of regdomains for each ontology term and outputs a tsv with
    the reg domain size.

    Inputs:
        merged_reg_by_term - Path to read from.
        rd_sizes - Path to write to.
        verbose - Weather to print progress.

    Outputs:
        Writes the regdom size to disk.
    """
    rd_size_dict = {}

    # Read in sizes
    with open(merged_reg_by_term,'r') as myFile:
        myReader = csv.reader(myFile, delimiter='\t')

        for row in myReader:
            mySize = int(row[2]) - int(row[1])
            myLabel = row[3]

            if myLabel not in rd_size_dict:
                rd_size_dict[myLabel] = mySize
            else:
                rd_size_dict[myLabel] += mySize

    # Write out sizes
    with open(rd_sizes,'w') as myFile:
        myWriter = csv.writer(myFile, delimiter='\t')

        for key in rd_size_dict:
            line = [key, rd_size_dict[key]]
            myWriter.writerow(line)

def gene_counts(ontoToGene, g_counts, verbose=False):
    """
    Reads in a list of genes for each ontology term and outputs a tsv with
    the count.

    Inputs:
        ontoToGene - Path to read from.
        g_counts - Path to write to.
        verbose - Weather to print progress.

    Outputs:
        Writes the regdom size to disk.
    """
    gene_count_dict = {}

    # Read in sizes

    if verbose == True:
        bytes_read = 0
        bar = -1
        file_size = os.path.getsize(ontoToGene)

    with open(ontoToGene,'r') as myFile:
        myReader = csv.reader(myFile, delimiter='\t')

        for row in myReader:
            if verbose == True:
                bytes_read += len(row)
                prog = 100*bytes_read / file_size
                if prog > bar:
                    sys.stdout.flush()
                    sys.stdout.write("Progress: {:02.0f}%%   \r".format(prog) )
                    bar += 1

            gene = row[0]

            if gene not in gene_count_dict:
                gene_count_dict[gene] = 1
            else:
                gene_count_dict[gene] += 1

    # Write out sizes
    with open(g_counts,'w') as myFile:
        myWriter = csv.writer(myFile, delimiter='\t')

        for key in gene_count_dict:
            line = [key, gene_count_dict[key]]
            myWriter.writerow(line)


def tfbs_by_term(merge_file, tfbs_file, out_file, settings, tmp_dir=None):
    """
    Create a tsv of the form tfbs X ontology term

    Inputs:
        merge_file - Input path for merged reg domain X ontology term bed file.
        tfbs_file - Path to bed file containing tfbs loci (not labeled).
        out_file - Output path for final tfbs region X ontology term bed file.
        settings - Settings dictionary.
        tmp_dir - Directory to store temp files.

    Outputs:
        Writes tfbs X ontology term bed file to disk.
    """
    if tmp_dir == None:
        rm_folder = True
        tmp_dir = '/'.join(out_file.split('/')[:-1])+'/tmp'

    if not os.path.exists(tmp_dir):
        os.makedirs(tmp_dir)

    # Overlap select with unique tfbs

    with open(out_file, 'w') as term_by_tf_file:
        overSelect = subprocess.Popen((settings['overlapSelect'],
                                       '-mergeOutput', merge_file,
                                       tfbs_file, 'stdout'),
                                      stdout=subprocess.PIPE)
        subprocess.call(('cut', '-f1-3,7'), stdin=overSelect.stdout,
                        stdout=term_by_tf_file)
        overSelect.wait()

def read_mat(data_file, given_reg_dict=None, given_label_dict=None, verbose=False):
    """
    This function reads in a tab delimited file of the form:
        chr reg_start reg_end label,
    and outputs dictionaries hashing the regions and the label.
    The label could be TF, ontology term, .... In addition it
    builds the sparse region X label matrix.

    Inputs:
        data_file - A tsv built from regdomains and label

    Outputs:
        reg_dict - A dict mapping regions to reg_id
        label_dict - A dict mapping labels to label_id
        sparse_data - A csr_matrix coding non-zero regions and labels
    """

    if given_reg_dict == None:
        reg_dict = {}
    else:
        reg_dict = given_reg_dict

    if given_label_dict == None:
        label_dict = {}
    else:
        label_dict = given_label_dict

    reg_count = 0
    label_count = 0

    NZ_row = []
    NZ_column = []

    if verbose == True:
        bytes_read = 0
        bar = -1
        file_size = os.path.getsize(data_file)

    with open(data_file,'r') as myFile:
        myReader = csv.reader(myFile, delimiter='\t')

        for row in myReader:
            myReg = '_'.join(row[0:3])
            myLabel = row[3]

            if given_reg_dict == None:
                if myReg not in reg_dict:
                    reg_dict[myReg] = reg_count
                    reg_count +=1
            else:
                assert myReg in given_reg_dict

            if given_label_dict == None:
                if myLabel not in label_dict:
                    label_dict[myLabel] = label_count
                    label_count +=1
            else:
                assert myLabel in given_label_dict

            NZ_row.append(reg_dict[myReg])
            NZ_column.append(label_dict[myLabel])

    row_dim = max(reg_dict.values()) + 1
    column_dim = max(label_dict.values()) + 1

    sparse_data = sparse.csr_matrix((np.ones_like(NZ_row),(NZ_row,NZ_column)),
                                    shape=(row_dim,column_dim))

    return reg_dict, label_dict, sparse_data

def read_dicts(data_file, given_reg_dict=None, given_label_dict=None,
               verbose=False):
    """
    This function reads in a tab delimited file of the form:
        chr reg_start reg_end ...,
    and outputs a dictionary hashing the regions.
    Same as above without the sparse matrix or label hash.

    Inputs:
        data_file - A tsv built from regdomains and label

    Outputs:
        reg_dict - A dict mapping regions to reg_id
    """

    if given_reg_dict == None:
        reg_dict = {}
    else:
        reg_dict = given_reg_dict

    reg_count = 0

    if verbose == True:
        bytes_read = 0
        bar = -1
        file_size = os.path.getsize(data_file)

    with open(data_file,'r') as myFile:
        myReader = csv.reader(myFile, delimiter='\t')

        for row in myReader:
            myReg = '_'.join(row[0:3])

            if given_reg_dict == None:
                if myReg not in reg_dict:
                    reg_dict[myReg] = reg_count
                    reg_count +=1
            else:
                assert myReg in given_reg_dict

    return reg_dict

def save_assembly(path,reg_dict,tf_dict,tf_sparse_data):
    """
    This function saves the processed assemply data to a compressed file.

    Inputs:
        path - Path to store data.
        reg_dict - Dictionary mapping PRISM regions to keys.
        tf_dict - Dictionary mapping TFs to keys.
        tf_sparse_data - Sparse region X TF matrix.

    Outputs:
        saves to file path.npz
    """

    tf_data = tf_sparse_data.data
    tf_indices = tf_sparse_data.indices
    tf_indptr = tf_sparse_data.indptr
    tf_shape = tf_sparse_data.shape

    np.savez_compressed(path,
                        reg_dict=reg_dict,
                        tf_dict=tf_dict,
                        tf_data=tf_data,
                        tf_indices=tf_indices,
                        tf_indptr=tf_indptr,
                        tf_shape=tf_shape)

def save_ontology(path,term_dict,term_sparse_data):
    """
    This function saves the processed ontology data to a compressed file.

    Inputs:
        path - Path to store data.
        term_dict - Dictionary mapping ontology terms to keys.
        term_sparse_data - Sparse region X term matrix.

    Outputs:
        saves to file path.npz
    """

    term_data = term_sparse_data.data
    term_indices = term_sparse_data.indices
    term_indptr = term_sparse_data.indptr
    term_shape = term_sparse_data.shape

    np.savez_compressed(path,
                        term_dict=term_dict,
                        term_data=term_data,
                        term_indices=term_indices,
                        term_indptr=term_indptr,
                        term_shape=term_shape)

# Building p p_values

def score_mat(term_num, tf_num):
    """
    Computes the matrix of summands in the score computation.

    Inputs:
        term_num - Number of ontology terms.
        tf_num - Number of transcription factors.

    Outputs:
        smat - Summand matrix (term_num, tf_num)
    """

    rank_vec = (np.arange(term_num)+1).reshape(term_num,1)
    smat = np.tile(np.arange(tf_num)+1,(term_num,1))*rank_vec
    smat = 1/np.sqrt(smat)

    return smat

def lnf(int_mat, int_score, lnf_hash={}):
    """
    Computes number of score combinations greater then or equal to int_score.

    Inputs:
        int_mat - Scalled matrix of possible scores.
        int_score - Scalled score
        lnf_hash - Dictionary of precomputed values.

    Outputs:
        lnf_val - Log of number of combinations with score greater or equal to score.
        lnf_hash - Dictionary of all computed values.
    """

    rev_mat = int_mat[:,::-1]
    term_num, tf_num = rev_mat.shape

    min_list = rev_mat[:,0].cumsum()
    max_list = rev_mat[:,-1].cumsum()

    if (term_num, int_score) in lnf_hash:
        lnf_val = lnf_hash[(term_num, int_score)]
    elif int_score <= min_list[term_num-1]:
        lnf_val = np.log10(tf_num)*term_num
    elif term_num == 1:
        lnf_val = np.log10((int_mat.ravel() >= int_score).sum())
    else:
        ok_list = int_mat[-1][(int_score - int_mat[-1] <= max_list[-2])]

        lnf_list = np.array([lnf(int_mat[:-1], int_score - i, lnf_hash=lnf_hash)[0] for i in ok_list])

        lnf_list_max = np.max(lnf_list)

        lnf_val = lnf_list_max + np.log10(np.power(10, lnf_list - lnf_list_max).sum())

    lnf_hash[(term_num, int_score)] = lnf_val

    return lnf_val, lnf_hash

def build_lsf(term_num, tf_num, lsf_out_dir, bins=1000):
    """
    Computes hash of all p-values with a given discretezation.

    Inputs:
        term_num - Number of ontology terms.
        tf_num - Number of transcription factors.
        lsf_out_dir - Directory to store lsf dicts.
    """

    sm = score_mat(term_num, tf_num)

    int_mat = np.round(sm*bins).astype(int)

    rev_mat = int_mat[:,::-1]

    min_list = rev_mat[:,0].cumsum()
    max_list = rev_mat[:,-1].cumsum()

    lnf_hash = {}

    for i in range(len(int_mat)):
        row_hash = {}
        last_percent = 0
        for j in range(min_list[i],max_list[i]+1):
            percent = (100*(j - min_list[i]))//(max_list[i]+1-min_list[i])
            if percent > last_percent:
                last_percent = percent
                sys.stdout.flush()
                sys.stdout.write("Row %d: Done: %d%%   \r" % (i, percent) )
            val, lnf_hash = lnf(int_mat[:i+1], j, lnf_hash=lnf_hash)
            row_hash[(i+1,j)] = val

        file_name = 'score_hash_' + str(i) + '.pkl'
        path = os.path.join(lsf_out_dir, file_name)

        with open(path, 'wb') as f:
            pickle.dump(row_hash, f, pickle.HIGHEST_PROTOCOL)


class Build_Ont_task():
    """
    This section builds the onotology data.

    Requires:



    Writes:


    Dir structure:

    $assembly_name
    |_ ontologies
    |   |_ $ont_name
    |       |_ regdoms.merge.bed
    |       |_ regdoms.sizes.tsv
    |       |_ gene.counts.tsv
    |_ $tfbs_name
        |_ tfbs.data.npz
        |_ tfbs.labeled.npz
        |_ tfbs.merge.npz
        |_ tfbs.unique.npz
        |_ ontologies
            |_ $ont_name
                |_ ont.data.npz
                |_ ont.merge.query.npz
                |_ terms.merge.bed
                |_ terms.unique.bed
    """
    def __init__(self, settings):
        self.settings = settings
        self.run_status = set()
    #
    def _build_dir_trees(self):
        build_dir_tree(self.settings.ontology_regdoms_base() + '/')
        build_dir_tree(self.settings.ontology_base() + '/')
        self.run_status.add('_build_dir_trees')
    def _create_regdoms_merge(self):
        '''This builds reg_domain (not_split) by term file - regdoms.merge.bed'''
        assert('_build_dir_trees' in self.run_status)
        merge_file=self.settings.merge_file()
        if os.path.exists(merge_file):
            print('{} exists, skipping.'.format(merge_file))
        else:
            print('Building reg domain by term file.')
            reg_region_by_term(
                self.settings.get('ont_path'),
                self.settings.get('assembly_loci'),
                merge_file,
                self.settings.get()
            )
        self.run_status.add('_create_regdoms_merge')
    def _create_rd_sizes(self):
        '''This builds reg_domain sizes file - regdoms.sizes.tsv'''
        assert('_create_regdoms_merge' in self.run_status)
        rd_sizes = self.settings.rd_sizes()
        merge_file=self.settings.merge_file()
        if os.path.exists(rd_sizes):
            print('{} exist, skipping.'.format(rd_sizes))
        else:
            print('Computing reg domain sizes.')
            reg_dom_size(merge_file, rd_sizes, verbose=True)
        self.run_status.add('_create_rd_sizes')
    def _create_g_counts(self):
        '''To Do: Implement gene counts here, so we can cut on it'''
        assert('_create_rd_sizes' in self.run_status)
        g_counts=self.settings.g_counts()
        if os.path.exists(g_counts):
            print('{} exist, skipping.'.format(g_counts))
        else:
            print('Computing annotation count for genes.')
            gene_counts(self.settings.get('ont_path'), g_counts, verbose=True)
        self.run_status.add('_create_g_counts')
    def _create_term_unique(self):
        '''This builds unique_tfbs labeled by term file - terms.unique.bed'''
        assert('_create_g_counts' in self.run_status)
        unique_term_file = self.settings.unique_term_file()
        if os.path.exists(unique_term_file):
            print('{} exists, skipping.'.format(unique_term_file))
        else:
            print('Building unique tfbs by term files.')
            tfbs_by_term(
                self.settings.merge_file(),
                self.settings.unique_tfbs_path(),
                unique_term_file,
                self.settings.get()
            )
        self.run_status.add('_create_term_unique')
    def _create_term_merge(self):
        '''This builds merge_tfbs labeled by term file - terms.merge.bed'''
        assert('_create_term_unique' in self.run_status)
        merge_term_file=self.settings.merge_term_file()
        if os.path.exists(merge_term_file):
            print('{} exists, skipping.'.format(merge_term_file))
        else:
            print('Building merge tfbs by term files.')
            tfbs_by_term(
                self.settings.merge_file(),
                self.settings.merge_tfbs_path(),
                merge_term_file,
                self.settings.get()
            )
        self.run_status.add('_create_term_merge')
    def _create_ontology_data(self):
        '''
        Build sparse file that contains term X merged region matrix, term_hash,
        merged_region_hash. Note, the term_hash might differ from that computed latter.

        Build sparse data file built from unique sites  - ont.data.npz
        This needs to load the previously computed tfbs data.
        '''
        assert('_create_term_merge' in self.run_status)
        ontology_data = self.settings.ontology_data()
        if os.path.exists(ontology_data):
            from whichtf.run import load_ontology
            print('{} exists, skipping.'.format(ontology_data))
            term_dict, _ = load_ontology(ontology_data)
            self.run_status.add('_create_ontology_data')
            return term_dict
        else:
            from whichtf.run import load_assembly
            print('Loading assembly data.')
            reg_dict, _, _ = load_assembly(self.settings.assembly_data())

            print('Building sparse matrix and hashes.')
            _, term_dict, term_sparse_data = read_mat(
                self.settings.unique_term_file(),
                given_reg_dict=reg_dict,
                verbose=True
            )
            print('Saving sparse matrix and hashes.')
            save_ontology(ontology_data, term_dict, term_sparse_data)
            self.run_status.add('_create_ontology_data')
            return term_dict
    def _create_ont_merge_query(self, term_dict):
        '''
        Build sparse query data file - ont.merge.query.npz
        This needs to build the reg_dict from tfbs.merge.bed. Also we want to
        use the same term_dict as above for consistency.
        '''
        assert('_create_ontology_data' in self.run_status)

        ont_query_file = self.settings.ontology_merge_query()
        if os.path.exists(ont_query_file):
            print('{} exists, skipping.'.format(ont_query_file))
        else:
            print('Reading in merged tfbs regions.')
            reg_dict = read_dicts(self.settings.merge_tfbs_path(), verbose=True)
            print('Building sparse matrix and hashes for genome wide query.')
            _, _, sparse_data = read_mat(
                self.settings.merge_term_file(),
                given_reg_dict=reg_dict,
                given_label_dict=term_dict,
                verbose=True
            )
            print('Saving sparse matrix and hashes.')
            save_assembly(ont_query_file, reg_dict, term_dict, sparse_data)
        self.run_status.add('_create_ont_merge_query')
    def build_Ont(self):
        self._build_dir_trees()
        self._create_regdoms_merge()
        self._create_rd_sizes()
        self._create_g_counts()
        self._create_term_unique()
        self._create_term_merge()
        term_dict = self._create_ontology_data()
        self._create_ont_merge_query(term_dict)


class Build_TFBS_task():
    def __init__(self, settings):
        self.settings         = settings
        self.run_status = set()
    def _build_dir_tree(self):
        build_dir_tree(self.settings.tfbs_base() + '/')
        self.run_status.add('_build_dir_tree')
    def _create_labeled_site(self):
        '''Creates a bed with tfbs labeled by tfbs name'''
        assert('_build_dir_tree' in self.run_status)
        labeled_site_file= self.settings.labeled_site_file()
        if os.path.exists(labeled_site_file):
            logger.info('{} exists, skipping.'.format(labeled_site_file))
        else:
            logger.info('Processing PRISM files.')
            process_tfbs(
                self.settings.get('tfbs_dir'), labeled_site_file
            )
        self.run_status.add('_create_labeled_site')
    def _create_unique_site(self):
        '''Creates a bed with unique tfbs without label.
        This is used for building ontology data.
        This is used at runtime in GREATER mode.'''
        assert('_create_labeled_site' in self.run_status)
        unique_site_file = self.settings.unique_tfbs_path()
        if os.path.exists(unique_site_file):
            logger.info('{} exists, skipping.'.format(unique_site_file))
        else:
            logger.info('Selecting unique entries.')
            unique_tfbs(self.settings.labeled_site_file(), unique_site_file)
        self.run_status.add('_create_unique_site')
    def _create_merge_site(self):
        '''Creates a bed with merged tfbs without label.
        This is used for building ontology data.
        (Only Needed in ... run modes).
        This is used at runtime ... To Do: Finish this.'''
        assert('_create_unique_site' in self.run_status)
        merge_site_file = self.settings.merge_tfbs_path()
        unique_site_file = self.settings.unique_tfbs_path()
        if os.path.exists(merge_site_file):
            logger.info('{} exists, skipping.'.format(merge_site_file))
        else:
            logger.info('Merging entries.')
            merge_tfbs(unique_site_file, merge_site_file, self.settings.get())
        self.run_status.add('_create_merge_site')
    def _save_to_npz(self):
        '''Saving data in convenient form for runs'''
        assert('_create_merge_site' in self.run_status)
        npz_file = self.settings.assembly_data()
        labeled_site_file = self.settings.labeled_site_file()
        if os.path.exists(npz_file):
            logger.info('{} exists, skipping.'.format(npz_file))
        else:
            logger.info('Building sparse matrix and hashes.')
            reg_dict, tf_dict, tf_sparse_data = read_mat(
                labeled_site_file, verbose=True)
            logger.info('Saving sparse matrix and hashes.')
            save_assembly(npz_file, reg_dict, tf_dict, tf_sparse_data)
            if not self.settings.get('leaveTrace'):
                os.remove(labeled_site_file)
        self.run_status.add('_save_to_npz')
    def build_TFBS(self):
        self._build_dir_tree()
        self._create_labeled_site()
        self._create_unique_site()
        self._create_merge_site()
        self._save_to_npz()
