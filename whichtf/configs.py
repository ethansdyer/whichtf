import sys, os, argparse, collections, tempfile, itertools
from logging import getLogger

logger = getLogger("configs.py")


class Config:
    def __init__(self):

        DEFAULT_DATA_PATH = os.path.realpath(__file__)
        DEFAULT_DATA_PATH = os.path.join(DEFAULT_DATA_PATH, '../../data')
        DEFAULT_DATA_PATH = os.path.abspath(DEFAULT_DATA_PATH)

        self.verbose_level = 0
        # list of positional args
        self._positional_args = []
        # configure default values
        self._d = collections.OrderedDict([])
        self._d['tmpdir']          = tempfile.gettempdir()
        self._d['settings']        = None
        self._d['leaveTrace']      = False
        self._d['data']            = DEFAULT_DATA_PATH
        # configure description of fields
        self._desc = collections.OrderedDict([])
        self._desc['tmpdir']          = 'temp directory'
        self._desc['settings']        = 'Load options from a setting file'
        self._desc['leaveTrace']      = 'Leave temporary files.'
        self._desc['data']            = 'Path to data directory.'
    def items(self):
        return self._d.items()
    def items_all(self):
        return itertools.chain([('verbose_level', self.verbose_level)], self.items())
    def update(self, iterable_items):
        '''
        Given a dict (any object with items() method), update the content
        '''
        if('verbose' in iterable_items):
            self.verbose_level = iterable_items['verbose']
        for key, val in iterable_items.items():
            if (key in self._d) and (key not in ['run_mode', 'verbose']):
                if(val is None or val == 'None'):
                    self._d[key] = None
                elif(isinstance(self._d[key], bool)):
                    self._d[key] = bool(val)
                elif(isinstance(self._d[key], float)):
                    self._d[key] = float(val)
                elif(isinstance(self._d[key], int)):
                    self._d[key] = int(val)
                elif(key.lower().endswith('file')):
                    self._d[key] = os.path.abspath(val)
                else:
                    self._d[key] = val
    def update_from_file(self, file):
        '''Read settings file (two column file) and update the object
        '''
        logger.info('reading config file from {}'.format(file))
        with open(file) as f:
            lines = [l for l in f.read().splitlines() if len(l) > 0 and l[0] != '#']
        self.update(dict([[x.split()[0], x.split()[1]] for x in lines]))

    def __str__(self):
        return '\n'.join(
            ['{}: {}'.format(key, val) for key, val in self.items_all()]
        )
    def tostr(self):
        return self.__str__
    def get_positional_args(self):
        return self._positional_args
    def get_desc(self, key):
        if(key in self._desc and self._desc[key] is not None):
            desc_str = str(self._desc[key])
        else:
            desc_str = ''
        if(key in self._d and self._d[key] is not None):
            default_str = ' (default: {})'.format(self._d[key])
        else:
            default_str = ''
        return desc_str + default_str
    def get(self, key = None):
        if(key is None):
            return self._d
        else:
            return self._d[key]

    ### Functions to get the path to files for datasets (ontology, PRISM, etc.)
    def assembly_base(self):
        return os.path.join(self.get('data'), self.get('assembly'))
    def tfbs_base(self):
        return os.path.join(self.assembly_base(), self.get('tfbs'))
    def ontology_regdoms_base(self):
        return os.path.join(self.assembly_base(), 'ontologies', self.get('myOnt'))
    def assembly_data(self):
        return os.path.join(self.tfbs_base(), 'tfbs.data.npz')
    def ontology_base(self):
        return os.path.join(self.tfbs_base(), 'ontologies', self.get('myOnt'))
    def ontology_data(self):
        return os.path.join(self.ontology_base(), 'ont.data.npz')
    def term_sizes(self):
        return os.path.join(self.ontology_regdoms_base(), 'regdoms.sizes.tsv')
    def unique_tfbs_path(self):
        return os.path.join(self.tfbs_base(), 'tfbs.unique.bed')
    def merge_tfbs_path(self):
        return os.path.join(self.tfbs_base(), 'tfbs.merge.bed')
    def chrom_sizes(self):
        return os.path.join(self.assembly_base(), 'chrom.sizes')


class Config_run(Config):
    def __init__(self):
        Config.__init__(self)

        # list of positional args
        self._positional_args += ['inFile', 'assembly']

        # configure default values
        self._d['inFile']          = None
        self._d['assembly']        = None
        self._d['outFile']         = None
        self._d['partialScore']    = False
        self._d['outPartialScore'] = None
        self._d['bedtools']        = 'bedtools'
        self._d['overlapSelect']   = 'overlapSelect'
        self._d['GREATER']         = 'GREATER'
        self._d['termNum']         = 100
        self._d['termMethod']      = 'bin'
        self._d['termFile']        = None
        self._d['myMin']           = 2
        self._d['myMax']           = 500
        self._d['tfbs']            = 'PRISM'
        self._d['myOnt']           = 'MGIPhenotype'
        self._d['equal_first_n']   = 1
        self._d['first_n_strict']  = False
        self._d['max_tf']          = 10
        self._d['before_hyp_jump'] = True
        self._d['before_bin_jump'] = True
        self._d['hyp_jump_thresh'] = 0.0
        self._d['bin_jump_thresh'] = 0.0
        self._d['max_rank']        = 1000
        self._d['min_score']       = 0.0
        self._d['pvBins']          = 1000

        # configure description of fields
        self._desc['inFile']          = 'Open chromatin bed file.'
        self._desc['assembly']        = 'Species assembly.'
        self._desc['outFile']         = 'Output file containing the ranked TFs.'
        self._desc['partialScore']    = 'Whether to save WhichTF partial score to a file. Please use --outPartialScore to change the output file name.'
        self._desc['outPartialScore'] = 'Output file containing the WhichTF partial scores. (Default: ${outFile%.tsv}.terms.tsv)'
        self._desc['bedtools']        = 'Path to bedtools executable.'
        self._desc['overlapSelect']   = 'Path to overlapSelect executable.'
        self._desc['GREATER']         = 'Path to GREATER executable.'
        self._desc['termNum']         = 'Number of ontology terms to use.'
        self._desc['termMethod']      = 'Method for generating term list.'
        self._desc['termFile']        = 'File to pull terms from if usting "file" method.'
        self._desc['myMin']           = 'Minimum number of genes for ontology term.'
        self._desc['myMax']           = 'Maximum number of genes for ontology term.'
        self._desc['tfbs']            = 'Transcription factor binding sites to use.'
        self._desc['myOnt']           = 'Ontology to use.'
        self._desc['equal_first_n']   = 'First n TFs must be common between hyper and binomial.'
        self._desc['first_n_strict']  = 'First n TFs must have identical ordering between hyper and binomial.'
        self._desc['max_tf']          = 'Number of TFs to use in computing p-value jump.'
        self._desc['before_hyp_jump'] = 'Only consider TFs below a jump in hyper p-value.'
        self._desc['before_bin_jump'] = 'Only consider TFs below a jump in binomial p-value.'
        self._desc['hyp_jump_thresh'] = 'Fold threshold for jump.'
        self._desc['bin_jump_thresh'] = 'Fold threshold for jump.'
        self._desc['max_rank']        = 'Maximum number of TFs to display.'
        self._desc['min_score']       = 'Score cutoff to display TF.'
        self._desc['pvBins']          = 'Score is rounded to 1/pvBins.'

    # Functions to get the path to files for datasets (query)
    def query_path(self):
        return os.path.join(self.get('inFile'))
    def in_file_name(self):
        return os.path.splitext(os.path.basename(self.query_path()))[0]
    def tmp_dir_make(self):
        self.run_tmp_dir = tempfile.mkdtemp(
            prefix='{}.{}.{}.'.format('WhichTF', 'tmp', self.in_file_name()),
            dir=self.get('tmpdir')
        )
        logger.debug('temp directory: {}'.format(self.run_tmp_dir))
    def tmp_dir_cleanup(self):
        assert(self.run_tmp_dir is not None)
        os.remove(self.unique_region_file())
        os.remove(self.merge_region_file())
        os.rmdir(self.run_tmp_dir)
    def unique_region_file(self):
        assert(self.run_tmp_dir is not None)
        return os.path.join(self.run_tmp_dir, self.in_file_name() + '.tfbs.unique.buz.bed')
    def merge_region_file(self):
        assert(self.run_tmp_dir is not None)
        return os.path.join(self.run_tmp_dir, self.in_file_name() + '.tfbs.merge.buz.bed')


class Config_buildTFBS(Config):
    def __init__(self):
        Config.__init__(self)

        # list of positional args
        self._positional_args += ['assembly', 'tfbs', 'tfbs_dir']

        # configure default values
        self._d['assembly']        = None
        self._d['tfbs']            = None
        self._d['tfbs_dir']        = None
        self._d['bedtools']        = 'bedtools'

        # configure description of fields
        self._desc['assembly']        = 'Species assembly.'
        self._desc['tfbs']            = 'Name to use for TFBS set.'
        self._desc['tfbs_dir']        = 'Path to TFBS set.'
        self._desc['bedtools']        = 'Path to bedtools executable.'

    # functions to provide file names
    def labeled_site_file(self):
        return os.path.join(self.tfbs_base(), 'tfbs.labeled.bed')


class Config_buildOnt(Config):
    def __init__(self):
        Config.__init__(self)

        # list of positional args
        self._positional_args += ['assembly', 'assembly_loci', 'myOnt', 'ont_path']

        # configure default values
        self._d['assembly']        = None
        self._d['assembly_loci']   = None
        self._d['myOnt']           = None
        self._d['ont_path']        = None
        self._d['tfbs']            = 'PRISM'
        self._d['getRegDoms']      = 'getRegDoms.py'
        self._d['bedtools']        = 'bedtools'
        self._d['overlapSelect']   = 'overlapSelect'


        # configure description of fields
        self._desc['assembly']        = 'Species assembly.'
        self._desc['assembly_loci']   = 'Path to assembly loci file.'
        self._desc['myOnt']           = 'Name to use for ontology.'
        self._desc['ont_path']        = 'Path to ontology.'
        self._desc['tfbs']            = 'Transcription factor binding sites to use.'
        self._desc['getRegDoms']      = 'Path to getRegDoms.py script.'
        self._desc['bedtools']        = 'Path to bedtools executable.'
        self._desc['overlapSelect']   = 'Path to overlapSelect executable.'

    # functions to provide file names
    def ontology_regdoms_base(self):
        return os.path.join(self.assembly_base(), 'ontologies', self.get('myOnt'))
    def merge_file(self):
        return os.path.join(self.ontology_regdoms_base(), 'regdoms.merge.bed')
    def rd_sizes(self):
        return os.path.join(self.ontology_regdoms_base(), 'regdoms.sizes.tsv')
    def g_counts(self):
        return os.path.join(self.ontology_regdoms_base(), 'gene.counts.tsv')
    def unique_term_file(self):
        return os.path.join(self.ontology_base(), 'terms.unique.bed')
    def merge_term_file(self):
        return os.path.join(self.ontology_base(), 'terms.merge.bed')
    def ontology_merge_query(self):
        return os.path.join(self.ontology_base(), 'ont.merge.query.npz')




class Config_buildPval(Config):
    def __init__(self):
        Config.__init__(self)

        # list of positional args
        self._positional_args += ['term_num', 'tf_num']

        # configure default values
        self._d['term_num']        = None
        self._d['tf_num']          = None
        self._d['bins']            = 1000

        # configure description of fields
        self._desc['term_num']        = 'Maximum number of ontology terms being used.'
        self._desc['tf_num']          = 'Maximum number of transcription factors being used.'
        self._desc['bins']            = 'P-value is computed at resolution 1/bins.'

    def lsf_dir(self):
        return os.path.join(self.get('data'), 'pval', str(self.get('tf_num')))



class Config_listAssemblies(Config):
    def __init__(self):
        Config.__init__(self)

class Config_listTFBS(Config):
    def __init__(self):
        Config.__init__(self)

class Config_listOnt(Config):
    def __init__(self):
        Config.__init__(self)


def get_commandline_parser(default_configs):
    desc = 'Identify relevent transcription factors within open chromatin.'
    sub_commands = collections.OrderedDict([
        ('run',            'Default run mode: WhichTF [options] in_file assembly.'),
        ('buildTFBS',      'Build set of TFBS to use with WhichTF.'),
        ('buildOnt',       'Build ontology database to use with WhichTF.'),
        ('buildPval',      'Build table of p-values.'),
        ('listAssemblies', 'List available assemblies.'),
        ('listTFBS',       'List available TFBS sets.'),
        ('listOnt',        'List available ontologies.'),
    ])

    parser = argparse.ArgumentParser(description=desc)
    subparsers = parser.add_subparsers()

    sub_ps = collections.OrderedDict([])
    for cmd, cmd_desc in sub_commands.items():
        sub_ps[cmd] = subparsers.add_parser(cmd, help=cmd_desc)
        sub_ps[cmd].set_defaults(run_mode=cmd)
        sub_ps[cmd].add_argument('--verbose', '-v', action='count')
        for argument, arg_default in default_configs[cmd].items():
            if(argument in default_configs[cmd].get_positional_args()):
                arg_str = str(argument)
            else:
                arg_str = '--{}'.format(argument)
            help_str = default_configs[cmd].get_desc(argument)
            if(isinstance(arg_default, bool) and arg_default):
                sub_ps[cmd].add_argument(
                    arg_str, help=help_str, action='store_false',
                )
            elif(isinstance(arg_default, bool) and (not arg_default)):
                sub_ps[cmd].add_argument(
                    arg_str, help=help_str, action='store_true',
                )
            elif(isinstance(arg_default, float)):
                sub_ps[cmd].add_argument(
                    arg_str, default=arg_default, help=help_str, type=float,
                )
            elif(isinstance(arg_default, int)):
                sub_ps[cmd].add_argument(
                    arg_str, default=arg_default, help=help_str, type=int,
                )
            elif(isinstance(arg_default, str)):
                sub_ps[cmd].add_argument(
                    arg_str, default=arg_default, help=help_str, type=str,
                )
            else:
                sub_ps[cmd].add_argument(
                    arg_str, default=arg_default, help=help_str,
                )
    return parser


def parse_argv(sys_argv):
    '''Given a sys_argv-like array, parse the command line argument into
    an instance of Config class and return it along with run_mode'''
    def insert_run_mode(argv, run_mode = 'run'):
        subparser_list = {
            'buildTFBS', 'buildOnt', 'buildPval', 'listAssemblies',
            'listTFBS', 'listOnt', '-h', '--help'
        }
        if len(subparser_list.intersection(set(argv))) == 0:
            return [run_mode] + argv
        else:
            return argv
    def get_configs(configs_dict = None):
        # this default configs are used to set the default variables of the command line parser
        default_configs = {
            'run': Config_run(),
            'buildTFBS': Config_buildTFBS(),
            'buildOnt': Config_buildOnt(),
            'buildPval': Config_buildPval(),
            'listAssemblies': Config_listAssemblies(),
            'listTFBS': Config_listTFBS(),
            'listOnt': Config_listOnt()
        }
        if(configs_dict is not None):
            for key, val in configs_dict.items():
                default_configs[key] = val
        return default_configs
    def parser_wrapper(configs, argv):
        # construct parser with content in "configs" as default
        parser = get_commandline_parser(configs)
        # parse argv
        args = parser.parse_args(argv)
        # update the config object with parsed cmdargs
        config = configs[args.run_mode]
        config.update(vars(args))
        return args.run_mode, config

    sys_argv_plus = insert_run_mode(sys_argv, run_mode = 'run')
    logger.debug(sys_argv_plus)
    run_mode, config = parser_wrapper(get_configs(), sys_argv_plus)
    if(config.get('settings') is not None):
        # if a setting file is specified, we will parse again the sys_argv with
        # the content of the setting file as the default for the parser
        config.update_from_file(config.get('settings'))
        config = parser_wrapper(get_configs(configs_dict={run_mode:config}), sys_argv_plus)
    return run_mode, config
