# WhichTF

* Version: 0.2

WhichTF identifies functionally important transcription factors in user-provided open chromatin regions.

Below, we denote the WhichTF base directory by `${WTF_HOME}`.

If the pre-install requirements are present then a full install and demo can be run with

```{bash}
cd ${WTF_HOME}
pip install -e .
./data/get_data.sh
WhichTF data/demo/ENCFF719GOE.bed hg19
```

This should take under 5 minutes on a standard desktop with output similar to.

```{text}
 Rank     TF    Score    Pvalue
    1   SPIB 8.624379 75.927365
    2  NFKB1 7.849356 89.537952
    3   RELB 4.967769 62.050696
    4   RELA 2.718572 32.019803
    5   SPIC 1.197411 11.460831
    6   SPI1 0.783814  9.269294
    7 ZFP410 0.778878 10.377766
    8  RUNX3 0.616621  8.500653
    9    REL 0.587175  8.551949
   10  STAT2 0.473659  6.788709
   11    WT1 0.298178  4.598139
   12  SNAI3 0.221920  4.229027
   13   ZEB2 0.185818  4.186882
   14   ATF6 0.176777  2.746768
   15   E2F5 0.125000  2.746029
   16  IKZF3 0.117359  4.429645
   17   ELF5 0.109109  2.752057
   18  SP100 0.106600  2.753583
   19   IRF9 0.071796  2.753583
   20  SNAI1 0.045408  2.753583
```

Please see below for more detailed instructions.

## Installation and downloading the reference data

### Pre-install requirements

* bedtools - [installation guide](http://bedtools.readthedocs.io/en/latest/content/installation.html)
* overlapSelect - (available as a part of [UCSC Genome Browser's utility tools](http://hgdownload.soe.ucsc.edu/downloads.html#source_downloads))
  * Executable binary files are available for [Linux 64-bit machines](http://hgdownload.soe.ucsc.edu/admin/exe/linux.x86_64/) and [Mac OSX](http://hgdownload.soe.ucsc.edu/admin/exe/macOSX.x86_64/).
* R (Version 3.5. Note, this does not have to be set as the default version)
* Python 3 (Tested with Python 3.6.4)

You can replicate the Python/R environment we used in the development with [`conda`](https://conda.io/projects/conda/en/latest/)

```{bash}
conda create -n whichtf -c r 'rpy2<=2.9.3' r-essentials r-base r-r.utils
conda activate whichtf
```

### WhichTF install

Please clone the repository and run a local install with `pip`.

```{bash}
git clone https://bitbucket.org/bejerano/whichtf.git
cd ${WTF_HOME}
pip install -e .
```

* **Note**: this `pip` command should also install dependencies (`NumPy`, `SciPy`, `Pandas`, and `rpy2` version 2.9.3)

### Download Ontology & TFBS data

WhichTF requires transcription factor binding sites and ontology data for the assembly of interest. We have a wrapper script to fetch the reference data:

```{bash}
cd ${WTF_HOME}/data
bash get_data.sh hg19 # we currently support hg19, hg38, mm9, and mm10
```

Alternatively, pre-computed data sets for four supported assemblies (hg19, hg38, mm9, and mm10) are available at [figshare](https://doi.org/10.6084/m9.figshare.12774539). Please download and unzip the tar archive and place it under `${WTF_HOME}/data`.

## How to run WhichTF?

You can run WhichTF via:

```{bash}
cd ${WTF_HOME}
WhichTF data_set assembly_name
```

For example, to run the demo file try

```{bash}
cd ${WTF_HOME}
WhichTF data/demo/ENCFF719GOE.bed hg19
```

To save the results to a file, please use `--outFile ` option.

```{bash}
cd ${WTF_HOME}
WhichTF data/demo/ENCFF719GOE.bed hg19 --outFile data/demo/ENCFF719GOE.whichtf.tsv
```

To investigate which ontology terms contribute to the TF ranking, please use `--partialScore` flag to let the program output WhichTF partial score. By default, the output file is `${outFile%.tsv}.terms.tsv` but you may specify the output file path with `--outPartialScore` flag.

Please see [`demos/demo_run.sh`](demos/demo_run.sh) for the example usage of the options.

### Example datasets and output files in this repository

We included 4 cell types shown in Table 1 of our manuscript as example input and output files in [`data/demos`](data/demos) directory.

| Sample  | Input (bed file)                                | Output                                                                                                             |
|---------|-------------------------------------------------|--------------------------------------------------------------------------------------------------------------------|
| B cells | [`ENCFF719GOE.bed`](data/demos/ENCFF719GOE.bed) | [TF ranks](data/demos/ENCFF719GOE.whichtf.tsv), [WhichTF partial scores](data/demos/ENCFF719GOE.whichtf.terms.tsv) |
| T cells | [`ENCFF861OSQ.bed`](data/demos/ENCFF861OSQ.bed) | [TF ranks](data/demos/ENCFF861OSQ.whichtf.tsv), [WhichTF partial scores](data/demos/ENCFF861OSQ.whichtf.terms.tsv) |
| Heart   | [`ENCFF176HSL.bed`](data/demos/ENCFF176HSL.bed) | [TF ranks](data/demos/ENCFF176HSL.whichtf.tsv), [WhichTF partial scores](data/demos/ENCFF176HSL.whichtf.terms.tsv) |
| Brain   | [`ENCFF318HIS.bed`](data/demos/ENCFF318HIS.bed) | [TF ranks](data/demos/ENCFF318HIS.whichtf.tsv), [WhichTF partial scores](data/demos/ENCFF318HIS.whichtf.terms.tsv) |

### Tested environments

* OS X Mojave, Python 3.6.4, R 3.5, rpy 2.9.3
* CentOS 7.2, Python 3.4.10, R 3.6, rpy2 2.7.0

## Development

* For current issues, future features, and a quick start to get oriented with the code. Please see the [development docs](docs/dev.md).

## Contact

* WhichTF was developed in the Bejerano lab (http://bejerano.stanford.edu/), by Ethan Dyer (`ethansdyer at gmail dot com`) and Yosuke Tanigawa (`yosuke <dot> tanigawa [at] alumni <dot> stanford <dot> edu`).

## References

* Y. Tanigawa\*, E. S. Dyer\*, G. Bejerano, WhichTF is dominant in your open chromatin data? bioRxiv, 730200 (2019). https://doi.org/10.1101/730200
